import {Module, State, Action, Getter, Mutation} from 'vue-decorators'
import api from '~/api/recipe'

@Module
export default class Cookbook {
  @State recipes = []
  @State selected = null

  @Getter fetchRecipes (state) {
    return state.recipes
  }

  @Getter fetchSelectedRecipe (state) {
    return state.selected
  }

  @Action readRecipe ({ state, commit }, id = null) {
    let recipe = null

    if (!id && state.recipes.length !== 0) {
      recipe = state.recipes[0]
    } else if (id) {
      recipe = state.recipes.find(r => r.id === id)
    }

    if (recipe) {
      commit('selectRecipe', recipe)
    }
  }

  @Action async renameRecipe ({ state, dispatch, commit }, id = null, newName = '') {
    if (id) {
      const recipe = state.recipes.find(r => r.id === id)
      recipe.name = newName

      if (recipe && await api.updateRecipe(recipe)) {
        dispatch('checkoutRecipe')
      }
    }
  }

  @Action async checkoutRecipes ({ state, dispatch, commit }) {
    commit('setRecipes', await api.getAllRecipes())

    if (!state.selected) {
      dispatch('readRecipe')
    }
  }

  @Action async addRecipe ({ dispatch, commit }, name = 'New Recipe') {
    if (await api.addRecipe(name)) {
      dispatch('checkoutRecipes')
    }
  }

  @Mutation setRecipes (state, recipes) {
    console.log(recipes)
    state.recipes = recipes
  }

  @Mutation selectRecipe (state, recipe) {
    state.selected = recipe
  }
}
