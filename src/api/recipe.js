import * as uuidv4 from 'uuid/v4'
import axios from 'axios'

const recipes = []

const http = axios.create({
  baseURL: process.env.API_URL,
  timeout: 1000
})

export default {
  async getAllRecipes () {
    const response = await http.get('/recipes')
    return response.data.recipes
  },

  async addRecipe (name) {
    return new Promise(
      resolve => setTimeout(() => {
        recipes.push({ id: uuidv4(), name })
        resolve(true)
      }, 2000)
    )
  }
}
